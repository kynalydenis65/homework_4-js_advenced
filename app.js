// Функція для отримання фільмів серії Зоряні війни
async function getStarWarsFilms() {
	try {
		const response = await axios.get('https://ajax.test-danit.com/api/swapi/films');
		const films = response.data;

		const filmsListDiv = document.getElementById('filmsList');

		films.forEach(async film => {
			const filmDiv = document.createElement('div');

			filmDiv.innerHTML = `
                        <h2>Episode ${film.episodeId}: ${film.name}</h2>
                        <p><strong>Opening Crawl:</strong> ${film.openingCrawl}</p>
                        <p><strong>Characters:</strong></p>
                        <ul id="characters-${film.episodeId}"></ul>
                    `;

			filmsListDiv.appendChild(filmDiv);

			// Отримання інформації про персонажів для кожного фільму
			for (const characterUrl of film.characters) {
				const characterResponse = await axios.get(characterUrl);
				const character = characterResponse.data;

				const charactersList = document.getElementById(`characters-${film.episodeId}`);
				const characterListItem = document.createElement('li');
				characterListItem.textContent = character.name;
				charactersList.appendChild(characterListItem);
			}
		});
	} catch (error) {
		console.error('Помилка під час отримання фільмів:', error.message);
	}
}

// Виклик функції для отримання фільмів та персонажів
getStarWarsFilms();
